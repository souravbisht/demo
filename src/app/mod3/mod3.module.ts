import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { M3DirDirective } from './m3-dir.directive';
import { M3pPipe } from './m3p.pipe';
import { M3cComponent } from './m3c/m3c.component';

@NgModule({
  declarations: [M3DirDirective, M3pPipe, M3cComponent],
  imports: [
    CommonModule
  ]
})
export class Mod3Module { }
