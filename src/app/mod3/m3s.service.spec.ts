import { TestBed } from '@angular/core/testing';

import { M3sService } from './m3s.service';

describe('M3sService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: M3sService = TestBed.get(M3sService);
    expect(service).toBeTruthy();
  });
});
