import { Component } from '@angular/core';
import { SharedService } from './../shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  count = 0;
  constructor(private sharedService: SharedService) {}
  changeE() {
    this.count += 1;
    console.log(this.count);
    this.sharedService.emitEmitter(this.count);
  }
}
