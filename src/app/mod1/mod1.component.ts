import { Component, OnInit } from '@angular/core';
import { SharedService } from './../shared.service';

@Component({
  selector: 'app-mod1',
  templateUrl: './mod1.component.html',
  styleUrls: ['./mod1.component.scss']
})
export class Mod1Component implements OnInit{
  title = 'demo';
  showData = [];
  listArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.sharedService.getEmitter()
    .subscribe((data) => {
      this.title = data;
    });
    this.showData = this.listArr.splice(0, 5);
  }

  onScroll(event) {
    const tracker = event.target;
    const limit = tracker.scrollHeight - tracker.clientHeight;
    if (event.target.scrollTop > (limit - 1)) {
      if (this.listArr.length) {
        const currData = this.showData;
        let newData = [];
        if (this.listArr.length >= 5) {
          newData = this.listArr.splice(0, 5);
        } else if (this.listArr.length > 0) {
          newData = this.listArr.splice(0, this.listArr.length);
        }
        this.showData = currData.concat(newData);
      }
    }
  }
}
