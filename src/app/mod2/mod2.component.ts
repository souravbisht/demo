import { Component, OnInit } from '@angular/core';
import { SharedService } from './../shared.service';

@Component({
  selector: 'app-mod2',
  templateUrl: './mod2.component.html',
  styleUrls: ['./mod2.component.scss']
})
export class Mod2Component implements OnInit{
  title = 'demo';
  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.sharedService.getEmitter()
    .subscribe((data) => {
      this.title = data;
    });
  }
}
