import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Mod1Component } from './mod1/mod1.component';
import { Mod2Component } from './mod2/mod2.component';

const routes: Routes = [
  {path: 'mod1', component: Mod1Component},
  {path: 'mod2', component: Mod2Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
