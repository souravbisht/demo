import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SharedService {
    public eventInvoke = new EventEmitter();

    emitEmitter(data) {
        this.eventInvoke.emit(data);
    }
    getEmitter() {
        return this.eventInvoke;
    }
}
